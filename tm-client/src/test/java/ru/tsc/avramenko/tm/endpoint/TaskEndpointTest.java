package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.marker.SoapCategory;

import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private final static SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final static SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final static ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final static ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final static TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final static TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Nullable
    private static SessionDTO session;

    @Nullable
    private TaskDTO task;

    @Nullable
    private ProjectDTO project;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession("User", "User");
        taskEndpoint.clearTask(session);
        projectEndpoint.clearProject(session);
    }

    @Before
    public void before() {
        taskEndpoint.createTask(session, "Task1", "TaskDesc1");
        projectEndpoint.createProject(session, "Project1", "ProjectDesc1");
        this.task = taskEndpoint.findTaskByName(session, "Task1");
        this.project = projectEndpoint.findProjectByName(session, "Project1");
    }

    @After
    public void after() {
        taskEndpoint.clearTask(session);
        projectEndpoint.clearProject(session);
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void bindTaskById() {
        taskEndpoint.bindTaskById(session, project.getId(), task.getId());
        @NotNull final TaskDTO taskNew = taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskNew.getProjectId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskAll() {
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskAll(session);
        Assert.assertNotNull(tasks);
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByProjectId() {
        taskEndpoint.bindTaskById(session, project.getId(), task.getId());
        @NotNull final TaskDTO taskNew = taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskNew.getProjectId());
        @NotNull final List<TaskDTO> list = taskEndpoint.findTaskByProjectId(session, taskNew.getProjectId());
        Assert.assertTrue(list.size()>0);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskById() {
        @NotNull final TaskDTO taskNew = taskEndpoint.finishTaskById(session, task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.COMPLETED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByName() {
        @NotNull final TaskDTO taskNew = taskEndpoint.finishTaskByName(session, task.getName());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.COMPLETED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTaskByIndex() {
        @NotNull final TaskDTO task = taskEndpoint.finishTaskByIndex(session, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusById() {
        @NotNull final TaskDTO taskNew = taskEndpoint.changeTaskStatusById(session, task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.NOT_STARTED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByName() {
        @NotNull final TaskDTO taskNew = taskEndpoint.changeTaskStatusByName(session, task.getName(), Status.NOT_STARTED);
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.NOT_STARTED, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByIndex() {
        @NotNull final TaskDTO task = taskEndpoint.changeTaskStatusByIndex(session, 0, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskById() {
        @NotNull final TaskDTO taskNew = taskEndpoint.startTaskById(session, task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.IN_PROGRESS, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByName() {
        @NotNull final TaskDTO taskNew = taskEndpoint.startTaskByName(session, task.getName());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.IN_PROGRESS, taskNew.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void startTaskByIndex() {
        @NotNull final TaskDTO task = taskEndpoint.startTaskByIndex(session, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateTaskById() {
        @NotNull final TaskDTO taskNew = taskEndpoint.updateTaskById(session, task.getId(), "NewTaskName", "NewTaskDesc");
        Assert.assertNotNull(taskNew);
        Assert.assertEquals("NewTaskName", taskNew.getName());
        Assert.assertEquals("NewTaskDesc", taskNew.getDescription());
    }

}