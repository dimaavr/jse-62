package ru.tsc.avramenko.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.IListenerService;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

import java.util.Collection;

@Component
public class ArgumentsDisplayListener extends AbstractListener {

    @Autowired
    private IListenerService listenerService;

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @Nullable
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list arguments.";
    }

    @Override
    @EventListener(condition = "@argumentsDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> arguments = listenerService.getArguments();
        for (final AbstractListener argument : arguments)
            System.out.println(argument.arg());
    }

}