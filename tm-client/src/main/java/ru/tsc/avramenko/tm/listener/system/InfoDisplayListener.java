package ru.tsc.avramenko.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.util.NumberUtil;

@Component
public class InfoDisplayListener extends AbstractListener {

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @Nullable
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String description() {
        return "Display system information.";
    }

    @Override
    @EventListener(condition = "@infoDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[INFO]");
        @NotNull final int availableProcessors = Runtime.getRuntime().availableProcessors();
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final long usedMemory = totalMemory - freeMemory;
        @NotNull final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}