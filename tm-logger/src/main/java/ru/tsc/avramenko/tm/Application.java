package ru.tsc.avramenko.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.avramenko.tm.bootstrap.Bootstrap;
import ru.tsc.avramenko.tm.configuration.LoggerConfiguration;

public class Application
{
    public static void main( String[] args )
    {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        LoggerConfiguration.class
                );
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }
}