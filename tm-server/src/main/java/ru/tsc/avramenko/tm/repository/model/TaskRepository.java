package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.model.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Query("SELECT e FROM Task e WHERE e.user.id = :userId and e.project.id = :projectId")
    @Nullable List<Task> findAllTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

    @Query("SELECT e FROM Task e WHERE e.user.id = :userId and e.name = :name")
    @Nullable Task findByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Query("SELECT e FROM Task e WHERE e.user.id = :userId")
    @Nullable List<Task> findByIndex(
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("DELETE FROM Task e WHERE e.user.id = :userId and e.name = :name")
    void removeByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Modifying
    @Query("DELETE FROM Task e")
    void clear();

    @Modifying
    @Query("DELETE FROM Task e WHERE e.user.id = :userId")
    void clear(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM Task e WHERE e.user.id = :userId and e.id = :id")
    @Nullable Task findById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Modifying
    @Query("DELETE FROM Task e WHERE e.user.id = :userId and e.id = :id")
    void removeById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM Task e WHERE e.user.id = :userId")
    @Nullable List<Task> findAllById(
            @Param("userId") @NotNull String userId
    );

    @Modifying
    @Query("UPDATE Task e SET e.project.id = NULL WHERE e.user.id = :userId AND e.project.id = :projectId")
    void unbindAllTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

}